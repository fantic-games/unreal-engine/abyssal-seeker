// Copyright 2023-2024 FaNtic, All rights reserved

using UnrealBuildTool;
using System.Collections.Generic;

public class AbyssalSeekerTarget : TargetRules
{
	public AbyssalSeekerTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Game;
		DefaultBuildSettings = BuildSettingsVersion.V2;

		ExtraModuleNames.AddRange( new string[] { "AbyssalSeeker" } );
	}
}
