// Copyright 2023-2024 FaNtic, All rights reserved

using UnrealBuildTool;
using System.Collections.Generic;

public class AbyssalSeekerEditorTarget : TargetRules
{
	public AbyssalSeekerEditorTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Editor;
		DefaultBuildSettings = BuildSettingsVersion.V2;

		ExtraModuleNames.AddRange( new string[] { "AbyssalSeeker" } );
	}
}
