// Copyright 2023-2024 FaNtic, All rights reserved

#include "ASBallBase.h"
#include "Components\StaticMeshComponent.h"

AASBallBase::AASBallBase()
{
	PrimaryActorTick.bCanEverTick = true;

	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Ball Mesh"));
	check(Mesh);
	SetRootComponent(Mesh);
}

void AASBallBase::BeginPlay()
{
	Super::BeginPlay();
}

void AASBallBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}
