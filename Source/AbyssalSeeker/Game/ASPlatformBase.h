// Copyright 2023-2024 FaNtic, All rights reserved

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ASPlatformBase.generated.h"

class AASBallBase;

UCLASS()
class ABYSSALSEEKER_API AASPlatformBase : public AActor
{
	GENERATED_BODY()
	
public:

	AASPlatformBase();

public:

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Default")
	UStaticMeshComponent* Mesh;

	UPROPERTY(EditDefaultsOnly, Category = "Default")
	TSubclassOf<AASBallBase> BallClass;

	UPROPERTY(EditDefaultsOnly, Category = "Default", meta = (ClampMin = "0", ClampMax = "100"))
	float Speed = 10;

protected:

	virtual void BeginPlay() override;

public:

	virtual void Tick(float DeltaTime) override;

private:

	AASBallBase* Ball;

};
