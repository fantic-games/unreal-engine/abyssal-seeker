// Copyright 2023-2024 FaNtic, All rights reserved

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ASBonusBase.generated.h"

UCLASS()
class ABYSSALSEEKER_API AASBonusBase : public AActor
{
	GENERATED_BODY()
	
public:

	AASBonusBase();

protected:

	virtual void BeginPlay() override;

public:

	virtual void Tick(float DeltaTime) override;

};
