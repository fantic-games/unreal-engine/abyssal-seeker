// Copyright 2023-2024 FaNtic, All rights reserved

#include "ASBonusBase.h"

AASBonusBase::AASBonusBase()
{
	PrimaryActorTick.bCanEverTick = true;
}

void AASBonusBase::BeginPlay()
{
	Super::BeginPlay();
}

void AASBonusBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}
