// Copyright 2023-2024 FaNtic, All rights reserved

#include "ASBlocksManagerBase.h"
#include "ASBlockBase.h"
#include "Components/BoxComponent.h"
#include "ASBlockBase.h"

DEFINE_LOG_CATEGORY(LogASBlocksManagerBase);

AASBlocksManagerBase::AASBlocksManagerBase()
{
	PrimaryActorTick.bCanEverTick = true;
}

void AASBlocksManagerBase::BeginPlay()
{
	Super::BeginPlay();

	if (!bUsePresetPatterns)
	{
		BlocksPatterns.Empty();
		BlocksPatterns.Add(static_cast<EBLocksPattern>(FMath::RandRange(0, 7)));
	}
	else if (BlocksPatterns.Num() == 0)
	{
		BlocksPatterns.Add(EBLocksPattern::Default);
		UE_LOG(LogTemp, Warning, TEXT("Used default pattern because BlocksPatterns are not specified!"));
	}

	for (EBLocksPattern Pattern : BlocksPatterns)
	{
		UE_LOG(LogASBlocksManagerBase, Display, TEXT("Generated Pattern: %i"), static_cast<uint8>(Pattern));
	}

	GenerateBlocksWithPattern(BlocksInRow, BlocksInColumn);
}

void AASBlocksManagerBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

float AASBlocksManagerBase::GetCenterIndex(int32 CurrentIndex, int32 LastIndex)
{
	return CurrentIndex - LastIndex / 2;
}

void AASBlocksManagerBase::GenerateBlocksWithPattern(int32 X, int32 Y)
{
	check(GetWorld());

	TArray<FTransform> BlockTransforms;
	float LocationX;
	float LocationY;

	FVector ManagerLocation = this->GetActorLocation();

	for (int32 i = 0; i < X; ++i)
	{
		LocationX = ManagerLocation.X + GetCenterIndex(i, X - 1) * OffsetX;

		for (int32 j = 0; j < Y; ++j)
		{
			LocationY = ManagerLocation.Y + GetCenterIndex(j, Y - 1) * OffsetY;

			if (PositionFitsPatterns(i, j, X - 1, Y - 1, BlocksPatterns.Array()))
			{
				BlockTransforms.Add(FTransform(FVector(LocationX, LocationY, ManagerLocation.Z)));
			}
		}
	}
	for (FTransform BlockTransform : BlockTransforms)
	{
		Block = GetWorld()->SpawnActor<AASBlockBase>(BlockClass, BlockTransform);
	}
}

bool AASBlocksManagerBase::PositionFitsPatterns(int32 RowIndex, int32 ColumnIndex, int32 MaxRowIndex, int32 MaxColumnIndex, TArray<EBLocksPattern> Patterns)
{
	for (EBLocksPattern Pattern : Patterns)
	{
		switch (Pattern)
		{
		case EBLocksPattern::Default:
			return true;
		case EBLocksPattern::EvenRow:
			if (RowIndex % 2 == 0) return true;
			break;
		case EBLocksPattern::OddRow:
			if (RowIndex % 2 == 1) return true;
			break;
		case EBLocksPattern::EvenColumn:
			if (ColumnIndex % 2 == 0) return true;
			break;
		case EBLocksPattern::OddColumn:
			if (ColumnIndex % 2 == 1) return true;
			break;
		case EBLocksPattern::X:
			if (PositionFitsXPatten(FIntPoint(RowIndex, ColumnIndex), MaxRowIndex, MaxColumnIndex)) return true;
			break;
		case EBLocksPattern::Square:
			if ((RowIndex == 0) || (RowIndex == MaxRowIndex) || (ColumnIndex == 0) || (ColumnIndex == MaxColumnIndex)) return true;
			break;
		case EBLocksPattern::Random:
			return FMath::RandBool();
		default:
			return false;
		}
	}
	return false;
}

TArray<FIntPoint> AASBlocksManagerBase::BresenhamLine(int32 x1, int32 y1, int32 x2, int32 y2)
{
	TArray<FIntPoint> LinePoints;

	const int32 deltaX = abs(x2 - x1);
	const int32 deltaY = abs(y2 - y1);
	const int32 signX = x1 < x2 ? 1 : -1;
	const int32 signY = y1 < y2 ? 1 : -1;
	int32 error = deltaX - deltaY;

	LinePoints.Add(FIntPoint(x2, y2));

	while (x1 != x2 || y1 != y2)
	{
		LinePoints.Add(FIntPoint(x1, y1));

		int32 error2 = error * 2;
		if (error2 > -deltaY)
		{
			error -= deltaY;
			x1 += signX;
		}
		if (error2 < deltaX)
		{
			error += deltaX;
			y1 += signY;
		}
	}
	return LinePoints;
}

bool AASBlocksManagerBase::PositionFitsXPatten(FIntPoint PointToCheck, int32 MaxRowIndex, int32 MaxColumnIndex)
{
	TArray<FIntPoint> XPattenPoints;

	XPattenPoints.Append(BresenhamLine(0, 0, MaxRowIndex, MaxColumnIndex));
	XPattenPoints.Append(BresenhamLine(MaxRowIndex, 0, 0, MaxColumnIndex));

	if (XPattenPoints.Contains(PointToCheck))
	{
		return true;
	}
	return false;
}
