// Copyright 2023-2024 FaNtic, All rights reserved

#include "ASPlatformBase.h"
#include "ASBallBase.h"

AASPlatformBase::AASPlatformBase()
{
	PrimaryActorTick.bCanEverTick = true;

	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Platform Mesh"));
	check(Mesh);
	SetRootComponent(Mesh);
}

void AASPlatformBase::BeginPlay()
{
	Super::BeginPlay();

	FVector InitialBallLocation = this->GetActorLocation() + FVector(50.0f, 0.0f, 0.0f);
	Ball = GetWorld()->SpawnActor<AASBallBase>(BallClass, FTransform(InitialBallLocation));
	Ball->AttachToActor(this, FAttachmentTransformRules::KeepWorldTransform);
}

void AASPlatformBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}
