// Copyright 2023-2024 FaNtic, All rights reserved

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ASBallBase.generated.h"

class UStaticMeshComponent;

UCLASS()
class ABYSSALSEEKER_API AASBallBase : public AActor
{
	GENERATED_BODY()
	
public:

	AASBallBase();

public:

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	UStaticMeshComponent* Mesh;

protected:

	virtual void BeginPlay() override;

public:

	virtual void Tick(float DeltaTime) override;

};
