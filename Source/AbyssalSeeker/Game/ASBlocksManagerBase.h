// Copyright 2023-2024 FaNtic, All rights reserved

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ASBlocksManagerBase.generated.h"

ABYSSALSEEKER_API DECLARE_LOG_CATEGORY_EXTERN(LogASBlocksManagerBase, Log, All);

class UBoxComponent;
class AASBlockBase;

UENUM(BlueprintType)
enum class EBLocksPattern : uint8
{
	Default = 0,
	EvenRow = 1,
	OddRow = 2,
	EvenColumn = 3,
	OddColumn = 4,
	X = 5,
	Square = 6,
	Random = 7
};


UCLASS()
class ABYSSALSEEKER_API AASBlocksManagerBase : public AActor
{
	GENERATED_BODY()

public:

	AASBlocksManagerBase();

public:

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<AASBlockBase> BlockClass;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (ClampMin = "1"))
	int32 BlocksInRow = 5;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (ClampMin = "1"))
	int32 BlocksInColumn = 5;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float OffsetX = 100;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float OffsetY = 100;

	UPROPERTY(EditDefaultsOnly)
	bool bUsePresetPatterns;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (EditCondition = "bUsePresetPatterns"))
	TSet<EBLocksPattern> BlocksPatterns;

	UBoxComponent* BlocksZone;

protected:

	virtual void BeginPlay() override;

public:

	virtual void Tick(float DeltaTime) override;

private:

	AASBlockBase* Block;
	TArray<AASBlockBase*> Blocks;

private:

	inline float GetCenterIndex(int32 CurrentIndex, int32 LastIndex);
	void GenerateBlocksWithPattern(int32 X, int32 Y);
	bool PositionFitsPatterns(int32 RowIndex, int32 ColumnIndex, int32 MaxRowIndex, int32 MaxColumnIndex, TArray<EBLocksPattern> Patterns);

	// Patterns Utils
	TArray<FIntPoint> BresenhamLine(int32 x1, int32 y1, int32 x2, int32 y2);
	bool PositionFitsXPatten(FIntPoint PointToCheck, int32 MaxRowIndex, int32 MaxColumnIndex);
};
