// Copyright 2023-2024 FaNtic, All rights reserved

#include "ASBlockBase.h"


AASBlockBase::AASBlockBase()
{
	PrimaryActorTick.bCanEverTick = true;
}

void AASBlockBase::BeginPlay()
{
	Super::BeginPlay();
}

void AASBlockBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}
