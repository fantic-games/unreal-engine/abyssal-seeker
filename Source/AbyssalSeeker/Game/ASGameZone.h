// Copyright 2023-2024 FaNtic, All rights reserved

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ASGameZone.generated.h"

ABYSSALSEEKER_API DECLARE_LOG_CATEGORY_EXTERN(LogASGameZone, Log, All);

class UBoxComponent;
class AASBlocksManagerBase;

UCLASS()
class ABYSSALSEEKER_API AASGameZone : public AActor
{
	GENERATED_BODY()
	
public:	

	AASGameZone();

public:

	UPROPERTY(EditDefaultsOnly, category = "Blocks")
	TSubclassOf<AASBlocksManagerBase> BlocksManagerClass;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, category = "Gameplay Zone")
	UBoxComponent* GameplayZone;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, category = "Gameplay Zone")
	UBoxComponent* BlocksZone;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, category = "Walls")
	UBoxComponent* WallRight;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, category = "Walls")
	UBoxComponent* WallLeft;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, category = "Walls")
	UBoxComponent* WallUp;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, category = "Walls")
	UBoxComponent* WallDown;

protected:

	virtual void BeginPlay() override;

public:	

	virtual void Tick(float DeltaTime) override;

private:

	float InitialGameplayBoxExtent = 50.0f;
	float WallPreScale = 0.1f;
	FVector GameplayBoxExtent = FVector(InitialGameplayBoxExtent);

	AASBlocksManagerBase* BlocksManager;

private:
	
	UBoxComponent* SetUpBoxComponent(FName SubObjectName, FVector InBoxExtent, ECollisionResponse CollisionResponse);
	virtual void PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent) override;
};
