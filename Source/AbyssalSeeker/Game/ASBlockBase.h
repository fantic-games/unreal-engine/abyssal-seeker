// Copyright 2023-2024 FaNtic, All rights reserved

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ASBlockBase.generated.h"

UCLASS()
class ABYSSALSEEKER_API AASBlockBase : public AActor
{
	GENERATED_BODY()
	
public:

	AASBlockBase();

public:

	UPROPERTY(EditDefaultsOnly)
	TMap<int32, UStaticMeshComponent*> StaticMeshByBlockLevel;

	UPROPERTY(BlueprintReadWrite, meta = (ClampMin = 0))
	int32 Level = 1;

protected:

	virtual void BeginPlay() override;

public:

	virtual void Tick(float DeltaTime) override;

};
