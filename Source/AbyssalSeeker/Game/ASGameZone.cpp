// Copyright 2023-2024 FaNtic, All rights reserved

#include "ASGameZone.h"
#include "ASBlocksManagerBase.h"
#include "Components/BoxComponent.h"
#include "Kismet/GameplayStatics.h"

DEFINE_LOG_CATEGORY(LogASGameZone);

AASGameZone::AASGameZone()
{
	PrimaryActorTick.bCanEverTick = true;

	// TODO: Make collision profiles to boxes https://forums.unrealengine.com/t/collision-presets-and-ccd-in-c/75426/7

	const ECollisionResponse GameplayZoneCollision = ECollisionResponse::ECR_Ignore;

	GameplayZone = SetUpBoxComponent(TEXT("Gameplay Zone"), GameplayBoxExtent, GameplayZoneCollision);
	SetRootComponent(GameplayZone);

	FVector GZCenter = GameplayZone->Bounds.Origin;
	FVector GZBoxExtent = GameplayZone->GetUnscaledBoxExtent();
	const FAttachmentTransformRules ATR = FAttachmentTransformRules::KeepRelativeTransform;

	FVector BlocksBoxExtent = FVector(GameplayBoxExtent.X, GameplayBoxExtent.Y, GameplayBoxExtent.Z);
	BlocksZone = SetUpBoxComponent(TEXT("Blocks Zone"), BlocksBoxExtent, GameplayZoneCollision);
	BlocksZone->SetRelativeLocation(FVector(GZCenter.X, GZCenter.Y, GZCenter.Z));
	BlocksZone->AttachToComponent(GameplayZone, ATR);

	const ECollisionResponse WallCollision = ECollisionResponse::ECR_Block;

	FVector WallBoxExtent = FVector(GZBoxExtent.X, GZBoxExtent.Y * WallPreScale, GZBoxExtent.Z);
	WallRight = SetUpBoxComponent(TEXT("Right Wall"), WallBoxExtent, WallCollision);
	WallRight->SetRelativeLocation(FVector(GZCenter.X, GZCenter.Y + GZBoxExtent.Y + WallRight->GetUnscaledBoxExtent().Y, GZCenter.Z));
	WallRight->AttachToComponent(GameplayZone, ATR);

	WallLeft = SetUpBoxComponent(TEXT("Left Wall"), WallBoxExtent, WallCollision);
	WallLeft->SetRelativeLocation(FVector(GZCenter.X, GZCenter.Y - GZBoxExtent.Y - WallLeft->GetUnscaledBoxExtent().Y, GZCenter.Z));
	WallLeft->AttachToComponent(GameplayZone, ATR);

	WallBoxExtent = FVector(GZBoxExtent.X * WallPreScale, GZBoxExtent.Y, GZBoxExtent.Z);
	WallUp = SetUpBoxComponent(TEXT("Up Wall"), WallBoxExtent, WallCollision);
	WallUp->SetRelativeLocation(FVector(GZCenter.X + GZBoxExtent.X + WallUp->GetUnscaledBoxExtent().X, GZCenter.Y, GZCenter.Z));
	WallUp->AttachToComponent(GameplayZone, ATR);

	WallDown = SetUpBoxComponent(TEXT("Death Wall"), WallBoxExtent, ECollisionResponse::ECR_Overlap);
	WallDown->SetRelativeLocation(FVector(GZCenter.X - GZBoxExtent.X - WallDown->GetUnscaledBoxExtent().X, GZCenter.Y, GZCenter.Z));
	WallDown->AttachToComponent(GameplayZone, ATR);
}

void AASGameZone::BeginPlay()
{
	check(GetWorld());
	BlocksManager = GetWorld()->SpawnActorDeferred<AASBlocksManagerBase>(BlocksManagerClass, FTransform(BlocksZone->Bounds.Origin));
	BlocksManager->BlocksZone = BlocksZone;
	BlocksManager->FinishSpawning(FTransform(BlocksZone->Bounds.Origin));
}

void AASGameZone::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

UBoxComponent* AASGameZone::SetUpBoxComponent(FName SubObjectName, FVector InBoxExtent, ECollisionResponse CollisionResponse)
{
	UBoxComponent* BoxComponent = CreateDefaultSubobject<UBoxComponent>(SubObjectName);

	if (!IsValid(BoxComponent))
	{
		UE_LOG(LogASGameZone, Error, TEXT("AASGameZone::SetUpBoxComponent: Box Component is Invalid!"));
		return nullptr;
	}

	BoxComponent->InitBoxExtent(InBoxExtent);
	BoxComponent->SetCollisionResponseToAllChannels(CollisionResponse);

	return BoxComponent;
}

void AASGameZone::PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent)
{
	if (PropertyChangedEvent.MemberProperty == NULL || PropertyChangedEvent.Property == NULL)
	{
		return;
	}

	if (PropertyChangedEvent.MemberProperty->NamePrivate == FName("RelativeScale3D"))
	{
		FVector Center = GameplayZone->Bounds.Origin;
		FVector Extent = GameplayZone->GetUnscaledBoxExtent();
		float RelativeScale;

		if (PropertyChangedEvent.Property->NamePrivate == FName("RelativeScale3D") || PropertyChangedEvent.Property->NamePrivate == FName("X") || PropertyChangedEvent.Property->NamePrivate == FName("Y"))
		{
			WallUp->SetRelativeScale3D(FVector::OneVector);
			WallUp->SetRelativeLocation(FVector(Center.X + Extent.X + WallUp->GetUnscaledBoxExtent().X, Center.Y, Center.Z));
			
			WallDown->SetRelativeScale3D(FVector::OneVector);
			WallDown->SetRelativeLocation(FVector(Center.X - Extent.X - WallDown->GetUnscaledBoxExtent().X, Center.Y, Center.Z));
			
			WallRight->SetRelativeScale3D(FVector::OneVector);
			WallRight->SetRelativeLocation(FVector(Center.X, Center.Y + Extent.Y + WallRight->GetUnscaledBoxExtent().Y, Center.Z));
			
			WallLeft->SetRelativeScale3D(FVector::OneVector);
			WallLeft->SetRelativeLocation(FVector(Center.X, Center.Y - Extent.Y - WallLeft->GetUnscaledBoxExtent().Y, Center.Z));
		}
		if (PropertyChangedEvent.Property->NamePrivate == FName("X") || PropertyChangedEvent.Property->NamePrivate == FName("Y"))
		{
			RelativeScale = WallUp->GetUnscaledBoxExtent().X / WallUp->GetScaledBoxExtent().X;
			WallUp->SetRelativeScale3D(FVector(RelativeScale, 1, 1));
			WallUp->SetRelativeLocation(FVector(Center.X + Extent.X + WallUp->GetUnscaledBoxExtent().X * RelativeScale, Center.Y, Center.Z));

			RelativeScale = WallDown->GetUnscaledBoxExtent().X / WallDown->GetScaledBoxExtent().X;
			WallDown->SetRelativeScale3D(FVector(RelativeScale, 1, 1));
			WallDown->SetRelativeLocation(FVector(Center.X - Extent.X - WallDown->GetUnscaledBoxExtent().X * RelativeScale, Center.Y, Center.Z));

			RelativeScale = WallRight->GetUnscaledBoxExtent().Y / WallRight->GetScaledBoxExtent().Y;
			WallRight->SetRelativeScale3D(FVector(1, RelativeScale, 1));
			WallRight->SetRelativeLocation(FVector(Center.X, Center.Y + Extent.Y + WallRight->GetUnscaledBoxExtent().Y * RelativeScale, Center.Z));

			RelativeScale = WallLeft->GetUnscaledBoxExtent().Y / WallLeft->GetScaledBoxExtent().Y;
			WallLeft->SetRelativeScale3D(FVector(1, RelativeScale, 1));
			WallLeft->SetRelativeLocation(FVector(Center.X, Center.Y - Extent.Y - WallLeft->GetUnscaledBoxExtent().Y * RelativeScale, Center.Z));
		}
	}

	Super::PostEditChangeProperty(PropertyChangedEvent);
}
