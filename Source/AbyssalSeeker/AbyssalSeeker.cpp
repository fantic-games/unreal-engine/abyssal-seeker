// Copyright 2023-2024 FaNtic, All rights reserved

#include "AbyssalSeeker.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, AbyssalSeeker, "AbyssalSeeker" );
