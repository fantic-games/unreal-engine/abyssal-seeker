// Copyright 2023-2024 FaNtic, All rights reserved

#include "ASPawnBase.h"
#include "AbyssalSeeker/Game/ASPlatformBase.h"
#include "AbyssalSeeker/Game/ASGameZone.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/PlayerStart.h"
#include "Kismet/GameplayStatics.h"
#include "Components/InputComponent.h"
#include "Components/BoxComponent.h"

AASPawnBase::AASPawnBase()
{
	PrimaryActorTick.bCanEverTick = true;

	CameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera"));
	CameraComponent->SetWorldRotation(FRotator(-90.0f, 0.0f, 0.0f));
	check(CameraComponent);
	SetRootComponent(CameraComponent);
}

void AASPawnBase::BeginPlay()
{
	Super::BeginPlay();

	AActor* PlayerStartActor = UGameplayStatics::GetActorOfClass(GetWorld(), APlayerStart::StaticClass());
	check(PlayerStartActor);
	this->SetActorLocation(PlayerStartActor->GetActorLocation());

	check(GetWorld());

	Platform = GetWorld()->SpawnActorDeferred<AASPlatformBase>(PlatformClass, InitialPlatformTransform);

	AASGameZone* GameZone = Cast<AASGameZone>(UGameplayStatics::GetActorOfClass(GetWorld(), AASGameZone::StaticClass()));
	check(GameZone);
	FVector WallDownCenter = GameZone->WallDown->Bounds.Origin;
	FVector WallDownExtent = GameZone->WallDown->Bounds.BoxExtent;

	FVector PlatformExtent = Platform->Mesh->Bounds.BoxExtent;

	FVector NewLocation = FVector(WallDownCenter.X + WallDownExtent.X + PlatformExtent.X, WallDownCenter.Y, WallDownCenter.Z);
	InitialPlatformTransform.SetLocation(NewLocation);

	Platform->FinishSpawning(InitialPlatformTransform);
	check(Platform);
}

void AASPawnBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AASPawnBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis("MoveHorizontal", this, &AASPawnBase::OnMoveHorizontal);
}

void AASPawnBase::OnMoveHorizontal(float InputAxisValue)
{
	if (!IsValid(Platform))
	{
		return;
	}

	FVector OffsetVector = FVector::ZeroVector;
	OffsetVector.Y = Platform->Speed;

	if (InputAxisValue > 0.0f)
	{
		Platform->AddActorWorldOffset(OffsetVector, true);
	}
	else if (InputAxisValue < 0.0f)
	{
		Platform->AddActorWorldOffset(-OffsetVector, true);
	}
}
