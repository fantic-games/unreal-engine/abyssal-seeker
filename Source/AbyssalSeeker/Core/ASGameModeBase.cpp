// Copyright 2023-2024 FaNtic, All rights reserved

#include "ASGameModeBase.h"
#include "ASPawnBase.h"
#include "AbyssalSeeker/Game/ASGameZone.h"
#include "GameFramework/PlayerStart.h"
#include "Components/CapsuleComponent.h"
#include "Kismet/GameplayStatics.h"

DEFINE_LOG_CATEGORY(LogASGameMode);

void AASGameModeBase::BeginPlay()
{
	check(GetWorld());

	SetUpPlayerStart();

	SpawnGameZone();

	APlayerController* PlayerController = GetWorld()->GetFirstPlayerController();
	check(PlayerController);
	AASPawnBase* Pawn = Cast<AASPawnBase>(PlayerController->GetPawn());
	check(Pawn);
}

void AASGameModeBase::SetUpPlayerStart()
{
	AActor* PlayerStartActor = UGameplayStatics::GetActorOfClass(GetWorld(), APlayerStart::StaticClass());

	if (!IsValid(PlayerStartActor))
	{
		UE_LOG(LogASGameMode, Error, TEXT("PlayerStart was not spawned!"));
		return;
	}

	if (bIsPlayerStartSetFromGameMode)
	{
		UCapsuleComponent* CapsuleComponent = PlayerStartActor->FindComponentByClass<UCapsuleComponent>();

		if (!CapsuleComponent)
		{
			UE_LOG(LogASGameMode, Error, TEXT("PlayerStart has no CapsuleComponent!"));
			return;
		}

		CapsuleComponent->SetMobility(EComponentMobility::Movable);
		PlayerStartActor->SetActorTransform(PlayerStartTransform);
		CapsuleComponent->SetMobility(EComponentMobility::Static);

		UE_LOG(LogASGameMode, Warning, TEXT("PlayerStart Transform was setted with GameMode"));
	}
}

void AASGameModeBase::SpawnGameZone()
{
	AActor* GameZoneActor = UGameplayStatics::GetActorOfClass(GetWorld(), AASGameZone::StaticClass());

	if (!IsValid(GetWorld()))
	{
		UE_LOG(LogASGameMode, Error, TEXT("AASGameModeBase::SpawnGameZone: World is Invalid!"));
		return;
	}

	if (!IsValid(GameZoneActor))
	{
		GameZoneActor = GetWorld()->SpawnActor<AASGameZone>(GameZoneClass, InitialGameZoneTransform);
		UE_LOG(LogASGameMode, Warning, TEXT("Game Zone was spawned with GameMode"));
	}
}
