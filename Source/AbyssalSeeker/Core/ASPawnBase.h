// Copyright 2023-2024 FaNtic, All rights reserved

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "ASPawnBase.generated.h"

class AASPlatformBase;
class UCameraComponent;

UCLASS()
class ABYSSALSEEKER_API AASPawnBase : public APawn
{
	GENERATED_BODY()

public:

	AASPawnBase();

public:

	UPROPERTY(BlueprintReadWrite)
	UCameraComponent* CameraComponent;

	UPROPERTY(EditDefaultsOnly, Category = "Settings|Platform")
	TSubclassOf<AASPlatformBase> PlatformClass;

protected:

	virtual void BeginPlay() override;

public:

	virtual void Tick(float DeltaTime) override;

	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

private:

	FTransform InitialPlatformTransform = FTransform::Identity;

	AASPlatformBase* Platform;

private:

	void OnMoveHorizontal(float InputAxisValue);

};
