// Copyright 2023-2024 FaNtic, All rights reserved

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "ASGameModeBase.generated.h"

ABYSSALSEEKER_API DECLARE_LOG_CATEGORY_EXTERN(LogASGameMode, Log, All);

class AASGameZone;

UCLASS()
class ABYSSALSEEKER_API AASGameModeBase : public AGameModeBase
{
	GENERATED_BODY()

public:

	UPROPERTY(EditDefaultsOnly, Category = "Settings|Player Start")
	bool bIsPlayerStartSetFromGameMode = false;

	UPROPERTY(EditDefaultsOnly, Category = "Settings|Player Start", meta = (EditCondition = "bIsPlayerStartSetFromGameMode"))
	FTransform PlayerStartTransform = FTransform::Identity;

	UPROPERTY(EditDefaultsOnly, Category = "Settings|Game Zone")
	FTransform InitialGameZoneTransform = FTransform::Identity;

	UPROPERTY(EditDefaultsOnly, Category = "Settings|Game Zone")
	TSubclassOf<AASGameZone> GameZoneClass;

protected:

	virtual void BeginPlay() override;

private:

	void SetUpPlayerStart();

	void SpawnGameZone();

};
